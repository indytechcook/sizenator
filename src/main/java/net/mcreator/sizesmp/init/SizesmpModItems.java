
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.sizesmp.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;

import net.minecraft.world.item.Item;

import net.mcreator.sizesmp.item.SmallatnatorItem;
import net.mcreator.sizesmp.item.RandomsizenatorItem;
import net.mcreator.sizesmp.item.BIggeranatorItem;
import net.mcreator.sizesmp.SizesmpMod;

public class SizesmpModItems {
	public static final DeferredRegister<Item> REGISTRY = DeferredRegister.create(ForgeRegistries.ITEMS, SizesmpMod.MODID);
	public static final RegistryObject<Item> B_IGGERANATOR = REGISTRY.register("b_iggeranator", () -> new BIggeranatorItem());
	public static final RegistryObject<Item> RANDOMSIZENATOR = REGISTRY.register("randomsizenator", () -> new RandomsizenatorItem());
	public static final RegistryObject<Item> SMALLATNATOR = REGISTRY.register("smallatnator", () -> new SmallatnatorItem());
}
