package net.mcreator.sizesmp;

import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import virtuoel.pehkui.api.ScaleData;
import virtuoel.pehkui.api.ScaleTypes;

import java.util.Random;

public class Scaler {
    public static final Random randomNumberGenerator = new Random();

    public static void increasePlayerSize(Entity entity) {
        final ScaleData data = ScaleTypes.BASE.getScaleData(entity);
        final float currentScale = data.getScale();
        final float newScale = currentScale + 1f;
        changePlayerScale(entity, newScale);
    }

    public static void decreasePlayerSize(Entity entity) {
        final ScaleData data = ScaleTypes.BASE.getScaleData(entity);
        final float currentScale = data.getScale();

        if (currentScale < 1) {
            return;
        }

        final float newScale = currentScale - 1f;
        changePlayerScale(entity, newScale);
    }

    public static void randomizePlayerScale(Entity entity) {
        if (!(entity instanceof Player)) {
            return;
        }

        final float scale = randomScaleSize(0.5f, 5f);
        changePlayerScale(entity, scale);
    }

    private static void changePlayerScale(Entity entity, float newSize) {
        final ScaleData data = ScaleTypes.BASE.getScaleData(entity);
        SizesmpMod.LOGGER.info("Current Scale {}", data.getScale());
        SizesmpMod.LOGGER.info("Changing scale to {}", newSize);
        data.setTargetScale(newSize);
        data.setPersistence(true);
    }

    private static float randomScaleSize(float min, float max) {
        if (min >= max) {
            throw new IllegalArgumentException("Invalid range [" + min + ", " + max + "]");
        }

        return min + randomNumberGenerator.nextFloat() * (max - min);
    }
}
